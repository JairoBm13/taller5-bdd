Feature: Opening the taximetro and calculate a fare

    Scenario: As a user I want to be able to open the taximetro screen from the drawer the first time i use the app
        Given I press "Paraderos"
        #button to remove the splash screen
        When I swipe left
        #to open te menu
        And I press "Taximetro"
        #go to tax fare calculator
        And I enter "9999" into input field number 1
        And I press the enter button
        And I press "Detalle de recargos"
        And I press "CERRAR" button
        Then I should see "$820,600"

    Scenario: As a user I want to be able to open the taximetro screen from the drawer and turn off the Door-Door extra fare
        #button to remove the splash screen
        Given I wait
        When I swipe left
        #to open te menu
        And I press "Taximetro"
        #go to tax fare calculator
        And I enter "9999" into input field number 1
        And I press the enter button
        And I press "Puerta-Puerta"
        Then I should see "$819,900"

    Scenario: As a user I want to be able to open the taximetro screen from the drawer with only night holyday fare
        #button to remove the splash screen
        Given I wait
        When I swipe left
        #to open te menu
        And I press "Taximetro"
        #go to tax fare calculator
        And I enter "9999" into input field number 1
        And I press the enter button
        And I press "Noct./Fest."
        Then I should see "$822,600"

    Scenario: As a user I want to be able to open the taximetro screen from the drawer with every extrafare
        #button to remove the splash screen
        Given I wait
        When I swipe left
        #to open te menu
        And I press "Taximetro"
        #go to tax fare calculator
        And I enter "9999" into input field number 1
        And I press the enter button
        And I press "Noct./Fest."
        And I press "Terminal"
        And I press "Aeropuerto"
        Then I should see "$827,200"