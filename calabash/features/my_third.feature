
Feature: Search for a route

    Scenario: As a user I want to check if a station is a valid origin
        Given I press "Paraderos"
        #button to remove the splash screen
        When I press "Viajar en Transmi, SITP o Taxi"
        And I press "tvOrigen"
        And I press "tvOrigen"
        And I enter text "Portal Suba" into field with id "etSearch"
        And I press "Zona C"
        Then I should see "Portal Suba"

    Scenario: As a user I want to check if a station is a valid destination
        Given I wait
        #button to remove the splash screen
        When I press "Viajar en Transmi, SITP o Taxi"
        And I press "tvDestino"
        And I press "tvDestino"
        And I enter text "Universidades" into field with id "etSearch"
        And I press "Universidades"
        And I press "Zona J"
        Then I should see "Universidades"

    Scenario: As a user i want to check routes between 2 points
        Given I wait
        #button to remove the splash screen
        When I press "Viajar en Transmi, SITP o Taxi"
        And I press "tvOrigen"
        And I press "tvOrigen"
        And I enter text "Portal Suba" into field with id "etSearch"
        And I press "Zona C"
        And I press "tvDestino"
        And I enter text "Universidades" into field with id "etSearch"
        And I press "Zona J"
        And I press "fabGo"
        Then I should see "Rutas sugeridas"