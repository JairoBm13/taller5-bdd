Feature: signup into losestudiantes
As an user I want to create an account myself within losestudiantes website in order to rate teachers
    Scenario Outline: Create account into losestudiantes

        Given I go to losestudiantes home screen
        When I open the Register screen
        And I put with <name1> and <lastname> and <email1> and <password1>
        And I select <university1> and <master> and <carrera1> and <doble> and <carrera2>
        And I check the conditions
        And I try to register
        Then I hope to see <result1>

        Examples:
            | name1 | lastname | email1           | password1 | university1              | master | carrera1             | doble | carrera2                             | result1 |
            | Test  | Man      | test@testman.com | 12345678  | Universidad de los Andes | yes    | Maestría en Diseño   | no    |                                      | Success |
# No se puede probar con webdriver| Test  | Man      | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño               | yes   | Ingeniería de Sistemas y Computación | Success |
            | Test  | Man      | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño               | no    |                                      | Success |

    Scenario Outline: Create account and fail by name o lastname

        Given I go to losestudiantes home screen
        When I open the Register screen
        And I put with <name1> and <lastname> and <email1> and <password1>
        And I select <university1> and <master> and <carrera1> and <doble> and <carrera2>
        And I check the conditions
        And I try to register
        Then Check emptyness input

        Examples:
            | name1 | lastname | email1           | password1 | university1              | master | carrera1 | doble | carrera2 | result1 |
            |       | Man      | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Success |
            | x     | Man      | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Success |
            | xx    | Man      | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Success |
            | Test  |          | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Success |
            | Test  | x        | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Success |
            | Test  | xx       | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Success |
            | Test  | Man      | test@testman.com |           | Universidad de los Andes | no     | Diseño   | no    |          | Success |

    Scenario Outline: Create account and fail by password validation

        Given I go to losestudiantes home screen
        When I open the Register screen
        And I put with <name1> and <lastname> and <email1> and <password1>
        And I select <university1> and <master> and <carrera1> and <doble> and <carrera2>
        And I check the conditions
        And I try to register
        Then Check password validity with <result1>

        Examples:
            | name1 | lastname | email1           | password1 | university1              | master | carrera1 | doble | carrera2 | result1                                         |
            | Test  | Man      | test@testman.com |           | Universidad de los Andes | no     | Diseño   | no    |          | Ingresa una contraseña                          |
            | Test  | Man      | test@testman.com | 1         | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |
            | Test  | Man      | test@testman.com | 12        | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |
            | Test  | Man      | test@testman.com | 123       | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |
            | Test  | Man      | test@testman.com | 1234      | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |
            | Test  | Man      | test@testman.com | 12345     | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |
            | Test  | Man      | test@testman.com | 123456    | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |
            | Test  | Man      | test@testman.com | 1234567   | Universidad de los Andes | no     | Diseño   | no    |          | La contraseña debe ser al menos de 8 caracteres |

Scenario Outline: Create account and fail by mail validation

        Given I go to losestudiantes home screen
        When I open the Register screen
        And I put with <name1> and <lastname> and <email1> and <password1>
        And I select <university1> and <master> and <carrera1> and <doble> and <carrera2>
        And I check the conditions
        And I try to register
        Then Check email pattern with <result1>

        Examples:
            | name1 | lastname | email1           | password1 | university1              | master | carrera1 | doble | carrera2 | result1                  |
            | Test  | Man      |                  | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Ingresa tu correo        |
            | Test  | Man      | testtestman.com  | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Ingresa un correo valido |
            | Test  | Man      | test@testmancom  | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Ingresa un correo valido |
            | Test  | Man      | testtestmancom   | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Ingresa un correo valido |

Scenario Outline: Create account and fail by mail validation

        Given I go to losestudiantes home screen
        When I open the Register screen
        And I put with <name1> and <lastname> and <email1> and <password1>
        And I select <university1> and <master> and <carrera1> and <doble> and <carrera2>
        And I try to register
        Then Check accept with <result1>

        Examples:
            | name1 | lastname | email1           | password1 | university1              | master | carrera1 | doble | carrera2 | result1                                  |
            | Test  | Man      | test@testman.com | 12345678  | Universidad de los Andes | no     | Diseño   | no    |          | Debes aceptar los términos y condiciones |
            