var { defineSupportCode } = require('cucumber');
var { expect } = require('chai');

defineSupportCode(({ Given, When, Then }) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if (browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When(/^I fill with (.*) and (.*)$/, (email, password) => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys(email);

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password)
  });

  When('I try to login', () => {
    var cajaLogIn = browser.element('.cajaLogIn');
    cajaLogIn.element('button=Ingresar').click()
  });

  Then('I expect to see {string}', result => {
    if (browser.isVisible('.aviso.alert.alert-danger')) {
      var alertText = browser.element('.aviso.alert.alert-danger').getText();
      expect(alertText).to.include(result);
    }
    else {
      browser.waitForVisible('.container', 5000);
    }
  });

  When('I open the Register screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When(/^I put with (.*) and (.*) and(.*) and (.*)$/, (name1, lastname, email1, password1) => {
    var cajaSignUp = browser.element('.cajaSignUp');
    browser.waitForVisible('input[name="nombre"]',5000);
    var nameInput = cajaSignUp.element('input[name="nombre"]');
    nameInput.click();
    nameInput.keys(name1);

    var lastnameInput = cajaSignUp.element('input[name="apellido"]');
    lastnameInput.click();
    lastnameInput.keys(lastname);

    var mailInput1 = cajaSignUp.element('input[name="correo"]');
    mailInput1.click();
    mailInput1.keys(email1);

    var passwordInput1 = cajaSignUp.element('input[name="password"]');
    passwordInput1.click();
    passwordInput1.keys(password1)
  });

  When(/^I select (.*) and (.*) and (.*) and (.*) and (.*)$/, (university1, master, carrera1, doble, carrera2) => {
    var cajaSignUp = browser.element('.cajaSignUp');

    if (master === "yes") {
      var checks = cajaSignUp.element('input[type="checkbox"]');
      checks.click();
    }
    else{
      if(doble === 'yes'){
        var checks = cajaSignUp.element('input[type="checkbox"]');
        checks.click();
        cajaSignUp.selectByVisibleText('select[name="idPrograma2"]', carrera2);
      } 
    }
    browser.waitForVisible('button=Registrarse', 60000);

    cajaSignUp.selectByVisibleText('select[name="idUniversidad"]', university1);

    cajaSignUp.selectByVisibleText('select[name="idPrograma"]', carrera1);
  });

  When('I check the conditions', () => {
    var cajaSignUp = browser.element('.cajaSignUp');
    var acepta = cajaSignUp.element('input[name="acepta"]');
    acepta.click();
  });

  When('I try to register', () => {
    var cajaSignUp = browser.element('.cajaSignUp');
    cajaSignUp.element('button=Registrarse').click()

  });

  Then('I expect to succed', () => {
    browser.waitForVisible('#cuenta', 15000);
    var boton = browser.element('#cuenta')
    boton.click();
    browser.element("a=Salir").click();
  });

  Then('I hope to see {string}', result1 => {
    browser.waitForVisible('button=Ok', 5000);
    browser.click('button=Ok');
  });

  Then('Check emptyness input', () => {
    expect(browser.elements(".has-errors"));
    expect(browser.elements(".has-errors"));

  });

  Then('Check password validity with {string}', (val) => {
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(val);
    expect(browser.elements(".has-errors"));
  });

  Then("Check email pattern with {string}", (val) =>{
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(val);
  });

  Then('Check accept with {string}', (val) => {
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(val);
  });
});